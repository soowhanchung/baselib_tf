import tensorflow as tf
import numpy as np
import warnings
import copy


warnings.simplefilter('default')


def fc_weights_init_xavier(in_dim, out_dim):
  stddev = np.reciprocal(np.sqrt((in_dim + 1) * out_dim))
  w = np.random.normal(scale=stddev, size=(in_dim, out_dim)).astype('f')
  b = np.random.normal(scale=stddev, size=out_dim).astype('f')
  return w, b


def fc_layer_init_xavier(in_node, dims, act_fn=tf.identity, name=''):
  """
  Fully connected feed-forward layer with randomly initialized network
  :param in_node : input node
  :param dims    : [input_dims, output_dims]
  :param act_fn  : activation function
  :param name    : name of the scope
  :return        : activated neural nodes
  """
  stddev = np.reciprocal(np.sqrt((dims[0] + 1) * dims[1]))
  with tf.name_scope('fclayer.' + name):
    w = tf.Variable(tf.random_normal(dims, stddev=stddev), name='weight')
    b = tf.Variable(tf.random_normal([dims[1]], stddev=stddev), name='bias')
    return act_fn(tf.add(tf.matmul(in_node, w), b)), [w, b]


def fc_layer_init_load(in_node, weight, bias, act_fn=tf.identity, name=''):
  """
  Fully connected feed-forward layer with pre-defined network
  :param in_node : input node
  :param dims    : [input_dims, output_dims]
  :param act_fn  : activation function
  :param name    : name of the scope
  :return        : activated neural nodes
  """
  with tf.name_scope('fclayer.' + name):
    w = tf.Variable(weight, name='weight')
    b = tf.Variable(bias, name='bias')
    return act_fn(tf.add(tf.matmul(in_node, w), b)), [w, b]


def mean_square_error(true, esti, name=''):
  """
  Cost function: mean-square-error (MSE)
  :param true: true data
  :param esti: estimated data
  :param name: name of the scope
  :return: evaluated mse tensor
  """
  with tf.name_scope('mse.' + name):
    return tf.reduce_mean(tf.square(tf.subtract(true, esti)))


def mean_euclidean_distance(true, esti, axis=1, name=''):
  """
  Cost function: averaged Euclidean distance
  :param true: true data
  :param esti: estimated data
  :param name: name of the scope
  :return: mean Euclidean distance
  """
  with tf.name_scope('dist.' + name):
    distance = tf.sqrt(tf.reduce_sum(tf.square(true - esti), axis=axis))
    return tf.reduce_mean(distance)


def logit_accuracy(label, logit, axis=1, name=''):
  """
  Cost function: Accuracy
  :param label: true one-hot labels
  :param logit: unscaled log-probabilities
  :param axis: axis of feature
  :param name: name of the scope
  :return: accuracy tensor
  """
  with tf.name_scope('acc.' + name):
    estimation = tf.equal(tf.argmax(logit, axis=axis), \
                          tf.argmax(label, axis=axis))
    return tf.reduce_mean(tf.cast(estimation, tf.float32))


def cross_entropy(label, logit, name=''):
  """
  Cost function: Cross entropy
  :param label: true one-hot labels
  :param logit: unscaled log-probabilities
  :param name: name of the scope
  :return: cross entropy tensor
  """
  with tf.name_scope('xent.' + name):
    xent = tf.nn.softmax_cross_entropy_with_logits(labels=label, logits=logit)
    return tf.reduce_mean(xent)


def gen_minibatch_idx(total, size):
  """
  Minibatch index generator
  :param total: total number of data
  :param size: size of minibatch
  :return: non-overlapping indices ranging from 0 to total, in minibatch size
  """
  n_minibatch = total // size
  mix_idx = np.random.permutation(total)
  minibatch_idx = mix_idx[:n_minibatch * size]
  minibatch_idx.shape = (n_minibatch, size)
  return minibatch_idx


def save_network_model(session, network, filename):
  fid = open(filename, 'wb')
  fid.write(len(network).to_bytes(4, 'little'))
  for layer in network:
    fid.write(len(layer).to_bytes(4, 'little'))
    for weight in layer:
      # for each network weight (could also be bias) item in layer
      weight_array = weight.eval(session)
      fid.write(len(weight_array.shape).to_bytes(4, 'little'))
      fid.write(np.array(weight_array.shape, dtype=np.int32).tobytes())
      fid.write(weight_array.tobytes())
  fid.close()


def gen_model(name, dims, in_node, out_node, act_fn, cost_fn):
  """
  Generating Fully-connected DNN Model, to be deprecated
  :param name: name scope of this model
  :param dims: dimension of hidden layers (3 hidden layers)
  :param in_node: placeholder for input data
  :param out_node: placeholder for output data
  :param act_fn: activation functions for hidden layers
  :param cost_fn: output cost functions
  :return: list of model related variables
  """
  # deprecation warning
  warnings.warn('dnn_model.gen_model is deprecated.', DeprecationWarning)
  # data dimensions
  data_dim = [in_node.get_shape()[1].value, out_node.get_shape()[1].value]
  # size of layer
  layer_size = [data_dim[0]]
  layer_size.extend(dims)
  layer_size.append(data_dim[1])
  # define model
  with tf.name_scope(name):
    # model settings - not liking fixed number of layers and range
    h1, net1 = fc_layer_init_xavier(in_node, layer_size[0:2], act_fn, 'h1')
    h2, net2 = fc_layer_init_xavier(h1, layer_size[1:3], act_fn, 'h2')
    h3, net3 = fc_layer_init_xavier(h2, layer_size[2:4], act_fn, 'h3')
    y, nety = fc_layer_init_xavier(h3, layer_size[3:], name='out')
    # cost and back-propagation operation settings
    network = [net1, net2, net3, nety]
    cost = cost_fn(out_node, y)
    bp = tf.train.AdamOptimizer(name='opt').minimize(cost)
  return [in_node, out_node, y, data_dim, cost, bp, network]


class FullyConnectedModel(object):
  def __init__(self, in_dim, out_dim, name='fc_model'):
    # initial input parameters
    self.in_node = tf.placeholder(tf.float32, [None, in_dim], name='input')
    self.out_node = tf.placeholder(tf.float32, [None, out_dim], name='output')
    self.name = name
    self.model_init = 0

    # additional input parameters
    self.criteria = []
    self.cost = None
    self.opt = None

    # derived parameters
    self.data_dims = [in_dim, out_dim]
    self.network = []
    self.hidden_layer_nodes = []
    self.out_layer = None

  def initialize_model(self, dims, hidden_act_fn, output_act_fn):
    if self.model_init:
      return
    in_dim = self.data_dims[0]
    in_node = self.in_node
    with tf.name_scope('network'):
      n_hidden_layers = len(dims)
      for i_layer in range(n_hidden_layers):
        layer_shape = [in_dim, dims[i_layer]]
        layer_node, net = fc_layer_init_xavier(in_node, layer_shape,
                                               hidden_act_fn, str(i_layer))
        self.network.append(net)
        self.hidden_layer_nodes.append(layer_node)
        in_node = layer_node
        in_dim = dims[i_layer]
      layer_shape = [in_dim, self.data_dims[1]]
      self.out_layer, net = fc_layer_init_xavier(in_node, layer_shape,
                                                 output_act_fn, 'out')
      self.network.append(net)

  def load_initialized_model(self, networks, hidden_act_fn, output_act_fn):
    if self.model_init:
      return
    in_node = self.in_node
    with tf.name_scope('network'):
      n_hidden_layers = len(networks) - 1
      for i_layer in range(n_hidden_layers):
        w_init = networks[i_layer][0]
        b_init = networks[i_layer][1]
        layer_node, net = fc_layer_init_load(in_node, w_init, b_init,
                                             hidden_act_fn, str(i_layer))
        self.network.append(net)
        self.hidden_layer_nodes.append(layer_node)
        in_node = layer_node
      w_init = networks[-1][0]
      b_init = networks[-1][1]
      self.out_layer, net = fc_layer_init_load(in_node, w_init, b_init,
                                               output_act_fn, 'out')
      self.network.append(net)

  def evaluation_criteria(self, cost_fns, opt_op):
    with tf.name_scope('evals'):
      for cost_fn in cost_fns:
        cost = cost_fn(self.out_node, self.out_layer, name=self.name)
        self.criteria.append(cost)
    with tf.name_scope('bp_op'):
      self.opt = opt_op.minimize(self.criteria[0])

  def training_setting(self, cost_fn, opt_op):
    old_name = self.__class__.__name__ + '.training_setting'
    new_name = self.__class__.__name__ + '.evaluation_criteria'
    message = '%s is deprecated, use %s instead.' % (old_name, new_name)
    warnings.warn(message, DeprecationWarning)
    self.cost = cost_fn(self.out_node, self.out_layer, self.name)
    self.opt = opt_op.minimize(self.cost)

