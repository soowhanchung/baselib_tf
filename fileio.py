import numpy as np


def read_binary_data(fid, nskip=0, ndata=-1, dtypein='f', dtypeout='f'):
  fid.seek(nskip, 1)
  raw_data = np.fromfile(fid, dtype=np.dtype(dtypein), count=ndata)
  return raw_data.astype(dtypeout)


def read_binary_as_onehot_label(fid, ndim, ndata=-1, itype='C', dtype='i4'):
  index = read_binary_data(fid, ndata=ndata, dtypein=dtype, dtypeout=dtype)
  if itype == 'F':
    index -= 1
  label = np.zeros((index.size, ndim))
  for i_label in range(index.size):
    label[i_label, index[i_label]] = 1
  return label


def read_batch_feature_as_list(filename, ndim, dtype='f'):
  fid = open(filename, 'rb')
  feature = []
  nbatch = int.from_bytes(fid.read(4), 'little')
  for ibatch in range(nbatch):
    nframe = int.from_bytes(fid.read(4), 'little')
    ndata = nframe * ndim
    raw_data = read_binary_data(fid, ndata=ndata, dtypein=dtype)
    raw_data.shape = (nframe, ndim)
    feature.append(raw_data)
  fid.close()
  return feature


def read_batch_feature_as_array(filename, ndim, dtype='f'):
  fid = open(filename, 'rb')

  # obtain full-size of the batch
  nbatch = int.from_bytes(fid.read(4), 'little')
  nframe_total = 0
  raw_data = []
  for ibatch in range(nbatch):
    # file size measuring sequence
    nframe = int.from_bytes(fid.read(4), 'little')
    raw_data.extend(fid.read(4 * ndim * nframe))
    nframe_total += nframe
  data = np.frombuffer(bytes(raw_data), dtypein=dtype)
  data.shape = (nframe_total, ndim)

  # finalize
  fid.close()
  return data


def save_numpy_as_binary(filename, data):
  fid = open(filename, 'wb')
  fid.write(data.tobytes())
  fid.close()
